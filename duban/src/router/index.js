import Vue from 'vue'
import vueRouter from 'vue-router'
import home from '@/pages/home/home.vue'
import list from '@/pages/List/List.vue'

Vue.use(vueRouter);

const routes = new vueRouter({
    routes: [{
        path: '/',
        name: 'home',
        component: home
    }, {
        path: '/list',
        name: 'list',
        component: list
    }]
})

export default routes;