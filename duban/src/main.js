import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import './assets/styles/reset.css'
import './assets/styles/border.css'

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')