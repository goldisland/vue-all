const menuList = [{
        name: '业务流程',
        index: '1',
        routerName: '/',
        children: [{
                name: '用户注册',
                index: '1-1',
                routerName: '/register'
            },
            {
                name: '任务池--查勘任务',
                index: '1-2',
                // routerName: '/taskPool_surveyTask',
                routerName: '/tableData'
            },
            {
                name: '调度管理--查勘任务',
                index: '1-3',
                routerName: '/dispatchManage_surveyTask'
            },
            {
                name: '查勘结果录入',
                index: '1-4',
                routerName: '/surveyResult_entering'
            },
            {
                name: '案件撤销',
                index: '1-5',
                routerName: '/caseRevocation'
            }, {
                name: '理算规则配置',
                index: '1-6',
                routerName: '/computationRole_deploy'
            }, {
                name: '任务池--理算确认',
                index: '1-7',
                routerName: '/taskPool_computationNotarize'
            }, {
                name: '调度管理--理算确认',
                index: '1-8',
                routerName: '/dispatchManage_computationNotarize'
            }, {
                name: '理算确认',
                index: '1-9',
                routerName: 'computationNotarize'
            }, {
                name: '理算书下载',
                index: '1-10',
                routerName: ''
            }, {
                name: '评议下发',
                index: '1-11',
                routerName: ''
            }, {
                name: '公示下发',
                index: '1-12',
                routerName: ''
            }, {
                name: '资金审批',
                index: '1-13',
                routerName: ''
            }, {
                name: '任务池--理赔计算',
                index: '1-14',
                routerName: ''
            }, {
                name: '调度管理--理赔计算',
                index: '1-15',
                routerName: ''
            }, {
                name: '理赔计算',
                index: '1-16',
                routerName: ''
            }, {
                name: '理赔结果查询',
                index: '1-17',
                routerName: ''
            }, {
                name: '异议',
                index: '1-18',
                routerName: ''
            },
        ]
    },
    {
        name: '报表管理',
        index: '2',
        routerName: '/',
        children: [{
                name: '理赔案件赔付数据',
                index: '2-1',
                routerName: ''
            },
            {
                name: '理赔案件作业时效',
                index: '2-2',
                routerName: ''
            },
            {
                name: '年龄性别出险分析',
                index: '2-3',
                routerName: ''
            }
        ]
    }
]

export default menuList;