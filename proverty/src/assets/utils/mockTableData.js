const data = [{
        "bh": 1,
        "xmid": "5463562",
        "xmmc": "测试名称",
        "Nsrsbh": "325423523542352345",
        "dwmc": "测试单位名称",
        "yskze": "89787.66",
        "srze": "345345.35",
        "kcze": "56566.56",
        "zze": "345345",
        "zsfs_mc": "计算方式1",
        "xmxs": "在建项目"
    },
    {
        "bh": 1,
        "xmid": "5463562",
        "xmmc": "测试名称",
        "Nsrsbh": "325423523542352345",
        "dwmc": "测试单位名称",
        "yskze": "0.00",
        "srze": "345345.35",
        "kcze": "56566.56",
        "zze": "345345",
        "zsfs_mc": "计算方式1",
        "xmxs": "在建项目"
    }
]

const headerData = [
    { name: '', desc: 'sel', width: '50', type: 'selection' },
    { name: '编号', desc: '', width: '60', type: 'index' },
    { name: '名称', desc: 'xmmc', tooltip: true },
    { name: '计算金额', desc: 'yskze', width: '150', click: true, url: '/xmtz/xmtzYskje', templet: function(d) { return d.yskze == '0.00' ? true : false } },
    { name: '计算价款', desc: 'srze', width: '150', click: true, sortable: true, url: '/xmtz/xmtzFwjsjk' },
    { name: '计算税金', desc: 'kcze', width: '150', style: 'text-align:right;' },
    { name: '唯一标识', desc: 'xmid', format: true, width: '150', edit: true, editType: 'input', },
    { name: '计算方式', desc: 'zsfs_mc', width: '150', edit: true, editType: 'select', editSelOptions: [{ label: '计算方式1', value: '计算方式1' }, { label: '计算方式2', value: '计算方式2' }], style: 'text-align:right;' },
    { name: '计算属性', desc: 'xmxs', width: '150', style: 'color:#409EFF;text-align:center;text-decoration: underline;' },
    { name: '操作', desc: 'jhqsnd', width: '200', btns: [{ name: '查看详情', btnType: 'primary', clickType: 'showXq' }, { name: '编辑', btnType: 'danger', clickType: 'editXq' }] },
]