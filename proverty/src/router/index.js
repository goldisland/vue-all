// 路由入口文件
import Vue from 'vue'
import vueRouter from 'vue-router'

Vue.use(vueRouter);

const resister = () =>
    import ( /* webpackChunkName: 'ImportFuncDemo' */ '../components/register');

const tableData = () =>
    import ( /* webpackChunkName: 'ImportFuncDemo' */ '../components/tableData');

const routes = [{
        path: '/',
        name: '404',
        component: () =>
            import ( /* webpackChunkName: 'ImportFuncDemo' */ '../components/404.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: resister
    },
    {
        path: '/tableData',
        name: 'tableData',
        component: tableData,
    }
]

export default new vueRouter({
    routes
});