const state = {
    name: '',
    roles: [],
    password: '',
    provinceAndCity: '', //省
    city: '', //市
    country: '', //区/县
    towns: '', //镇
    village: '', //村
}

const mutations = {
    SET_NAME: (state, name) => {
        state.name = name;
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles;
    },
    SET_PASSWORD: (state, password) => {
        state.password = password;
    },
    SET_PROVINCEANDCITY: (state, provinceAndCity) => {
        state.provinceAndCity = provinceAndCity;
    },
    SET_CITY: (state, city) => {
        state.city = city;
    },
    SET_COUNTRY: (state, country) => {
        state.country = country;
    },
    SET_TOWNS: (state, towns) => {
        state.towns = towns;
    },
    SET_VILLAGE: (state, village) => {
        state.village = village;
    },
}

const actions = {
    CHANGE_NAME({ commit, dispatch }, name) {
        return new Promise((resolve, reject) => {
            commit('SET_NAME', name);
        })
    }
}